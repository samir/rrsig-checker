#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

if ! [ -f "$DIR/config" ]; then
    >&2 echo "config not found at \"$DIR/config\""
    exit 1
fi

. "$DIR/config"

cd "$DIR"
for domain in $DOMAINS; do
    MSG=$(/usr/bin/perl check_zone_rrsig_expiration -Z $domain -W $WARNING -C $CRITICAL)
    RET=$?
    # send to mail to $MAIL if the test did not sucess
    if [ $RET -ge 1 ]; then
        /usr/sbin/sendmail $MAIL <<EOF
From: nodody+rrsig-checker@grenade.genua.fr
To: $MAIL
Subject: DNSSEC RRSIG Expiration for zone $domain

$MSG
EOF
    fi
    if [ $RET -eq 0 ]; then
        on_success "$domain" "$MSG"
    fi
    if [ $RET -eq 1 ]; then
        on_warning "$domain" "$MSG"
    fi
    if [ $RET -eq 2 ]; then
        on_critical "$domain" "$MSG"
    fi
done
